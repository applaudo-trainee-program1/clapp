import Image from 'next/image';
import Link from 'next/link';
import { Routes } from '../../routes/routes';
import { CardProps } from './types';
import { motion } from 'framer-motion';
import { getScoreClassName } from '../../utils/utils';
import LikeBar from '../LikeBar/LikeBar';

const Card = ({ id, title, img, category, score, itemVariant }: CardProps) => {
  return (
    <motion.div
      className='relative'
      variants={itemVariant}>
      <Link
        className="flex flex-col items-center bg-white-glass p-4 rounded-lg"
        href={`${Routes[category]}/${id}`}
      >
        <article className="flex flex-col max-w-[200px]">
          <div className='relative'>
            <Image
              className='rounded-lg aspect-[2/3]'
              src={img ?? ''}
              alt={title}
              width={200}
              height={300}
            />
            <p
              className={`flex justify-center items-center h-10 w-10 text-white font-semibold 
            rounded-full absolute bottom-[-1.25rem] left-2 ${getScoreClassName(score)}`}
            >{score.toFixed(1) ?? 'NR'}</p>
          </div>
          <h4 role="heading"
            className={'text-center text-lg font-bold pt-6 line-clamp-2 min-h-[80px]'}
          >
            {title}
          </h4>
        </article>
      </Link>
      <div className='absolute top-4 right-4'>
        <LikeBar altImg='like' mediaType={category === 'MOVIES' ? 'movie' : 'tv'} id={Number(id)}/>
      </div>
    </motion.div>
  );
};

export default Card;
