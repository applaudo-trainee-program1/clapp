import { Routes } from '../../routes/routes';
import { Variants } from 'framer-motion';

export type CardProps = {
  category: Extract< keyof typeof Routes, 'MOVIES' | 'TV_SHOWS'>,
  id: number | string
  img?: string,
  title: string,
  score: number,
  itemVariant?: Variants
}

export type CategoryLowercase = Lowercase<CardProps['category']>