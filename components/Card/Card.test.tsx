import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import Card from './Card';



const cardPropMocked = {
  'category': 'TV_SHOWS' as const,
  'id': 119051,
  'title': 'Wednesday',
  'score': 8.7,
  'img': 'https://image.tmdb.org/t/p/original/9PFonBhy4cQy7Jz20NpMygczOkv.jpg'
};

describe('Test card interactivity', () => {
  it('card was rendered', async () => {
    render(
      <Provider store={store}>
        <Card
          category={cardPropMocked.category}
          id={cardPropMocked.id}
          img={cardPropMocked.img}
          title={cardPropMocked.title}
          score={cardPropMocked.score}
        />
      </Provider>
    );
    const title = screen.getByText(cardPropMocked.title);
    expect(title).toBeInTheDocument();
  });
});
