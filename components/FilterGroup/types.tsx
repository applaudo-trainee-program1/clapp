export type FilterGroupProps = {
  section: 'MOVIES' | 'TV_SHOWS'
}
