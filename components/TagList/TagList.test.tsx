import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import props from '../../mocks/props/tagList.json';
import TagList from './TagList';

describe('Test TagList', () => {
  it('Should render title & list items', async () => {
    render(
      <TagList 
        title={props.title}
        list={props.list}
      />
    );
    const title = screen.getByText(props.title);
    const itemList = screen.getByText(props.list[0]);
    expect(title).toBeInTheDocument();
    expect(itemList).toBeInTheDocument();
  });
});
