import { CardProps } from '../Card/types';

export type GridLayoutProps = {
  loading: boolean,
  dataArray: CardProps[] | null,
  titleLayout: string,
  section: 'MOVIES' | 'TV_SHOWS' | 'BOTH'
}