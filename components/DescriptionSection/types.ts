export type DescriptionSectionProps = {
  title: string,
  description?: string
}