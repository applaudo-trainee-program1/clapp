import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import ErrorCard from './ErrorCard';

describe('Test ErrorCard rendering', () => {
  it('Should render buttons to home & search sections', async () => {
    render(
      <ErrorCard />
    );
    const homeButton = screen.getByText(/home/i);
    const searchButton = screen.getByText(/search/i);
    expect(homeButton).toBeInTheDocument();
    expect(searchButton).toBeInTheDocument();
  });
  it('Should not render buttons to home & search sections', async () => {
    render(
      <ErrorCard showLinks={false}/>
    );
    const homeButton = screen.queryByText(/home/i);
    const searchButton = screen.queryByText(/search/i);
    expect(homeButton).not.toBeInTheDocument();
    expect(searchButton).not.toBeInTheDocument();
  });
});
