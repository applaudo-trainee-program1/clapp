import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import Header from './Header';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
    };
  },
}));

describe('Test Header', () => {
  it('Should render a navbar', async () => {
    render(
      <Provider store={store}>
        <Header />
      </Provider>
    );
    const search = screen.getByText(/search/i);
    expect(search).toBeInTheDocument();
  });
});
