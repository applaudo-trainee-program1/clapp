import Image from 'next/image';
import Link from 'next/link';
import  { useState } from 'react';
import { close, menu } from '../../public/images';
import { Routes } from '../../routes/routes';
import { Navbar } from '../';

const Header = () => {
  const [menuOn, setMenuOn] = useState(false);
  const toggleMenu = () => setMenuOn(prev => !prev);
  const handleCloseMenu = () => setMenuOn(false);
  
  return (
    <header className='w-full flex justify-between items-center h-16'>
      <Link className='flex justify-center items-center gap-4 cursor-pointer' href={Routes.HOME}>
        <h1 className='text-5xl font-bold'>Clapp</h1>
      </Link>
      <button className='flex justify-center items-center gap-4 bg-transparent h-3/4 cursor-pointer 
      invert sm:hidden' 
      type='button'
      onClick={toggleMenu}>
        {!menuOn &&
        <Image width={32} height={32} src={menu} alt="Menu button"/>}
        {menuOn &&
        <Image 
          width={32} height={32} 
          className='scale-[0.8]'src={close}
          alt="Menu button"/>}
      </button>
      {menuOn &&
        <div className='bg-transparent absolute inset-0 top-16 sm:hidden'>
          <Navbar
            handleCloseMenu={handleCloseMenu}
          />
        </div>
      }
      <div className='hidden sm:flex sm:flex-row sm:items-center sm:bg-transparent sm:relative 
      sm:p-0 sm:inset-auto'>
        <Navbar />
      </div>
    </header>
  );
};

export default Header;
