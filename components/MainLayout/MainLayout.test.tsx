import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import MainLayout from './MainLayout';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
    };
  },
}));

describe('Test MainLayout', () => {
  it('Should render a children', async () => {
    render(
      <Provider store={store}>
        <MainLayout>
          <span>
          Test layout
          </span>
        </MainLayout>
      </Provider>
    );
    const children = screen.getByText('Test layout');
    expect(children).toBeInTheDocument();
  });
});
