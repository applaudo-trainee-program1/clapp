
import { Header } from '../';
import { MainLayoutProps } from './types';

const MainLayout = ({children}:MainLayoutProps) => {
  return (
    <div className={`flex min-h-screen w-full px-4 bg-dark justify-center text-cyan-50 relative 
    overflow-auto`}>
      <div className="max-w-5xl w-full">
        <Header />
        {children}
      </div>
    </div>
  );
};
export default MainLayout;