/* eslint-disable no-unused-vars */
export type InputDebounceProps = {
  initialValue: string,
  handleUpdate: (value:string) => void,
  max?: number,
  min?: number,
  title: string,
  type: 'INTEGER_RANGE' | 'TEXT',
  containerclassName?: string,
  inputClassname?: string
} 
