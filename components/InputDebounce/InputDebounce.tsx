import { useState, useEffect } from 'react';
import { DEFAULT_DEBOUNCE_TIMEOUT } from '../../config';
import { validateIntRangeBeforeDebounce, 
  validateIntRangeWhileDebounce } from '../../utils/validations';
import { InputDebounceProps } from './types';

const InputDebounce = ({initialValue, handleUpdate, title, type, max, min, 
  containerclassName, inputClassname}:InputDebounceProps) => {
  const [valueWithoutDebounce, setValueWithoutDebounce] = useState<string>(initialValue);

  const setValueFromInput = (e:React.ChangeEvent<HTMLInputElement>) => {
    const validatedInput = type==='INTEGER_RANGE' 
      ? validateIntRangeBeforeDebounce(e.target.value) 
      : e.target.value;
    setValueWithoutDebounce(validatedInput);
  };

  useEffect(() => {
    let timer: null | NodeJS.Timeout = null;
    if (initialValue !== valueWithoutDebounce)
      timer = setTimeout(() => {
        const validatedInputValue = type==='INTEGER_RANGE' 
          ? validateIntRangeWhileDebounce(valueWithoutDebounce, max || 0, min || 0)
          : valueWithoutDebounce;
        setValueWithoutDebounce(validatedInputValue);
        if(valueWithoutDebounce === validatedInputValue)
          handleUpdate(validatedInputValue);
      }, DEFAULT_DEBOUNCE_TIMEOUT);
    if (!valueWithoutDebounce && timer !== null) clearTimeout(timer);

    return () => { if (timer !== null) clearTimeout(timer); };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [valueWithoutDebounce]);
  
  return (
    <div className={ containerclassName ? containerclassName : 'flex items-center gap-2 h-10'}>
      <label className='cursor-pointer select-none' htmlFor="debounceInput">{ title }</label>
      <input
        id="debounceInput"
        className={inputClassname 
          ? inputClassname 
          : 'p-2 text-base rounded-lg text-black w-20 text-center'}
        type="text"
        placeholder={ title }
        onInput={setValueFromInput}
        value={valueWithoutDebounce}
      />
    </div>
  );
};
export default InputDebounce;