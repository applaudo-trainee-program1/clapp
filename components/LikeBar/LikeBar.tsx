import Image from 'next/image';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import genericAxiosInstance from '../../apiInstances/genericApi';
import { useFetch } from '../../hooks';
import { like } from '../../public/images';
import { setDisLike, setLike } from '../../redux/slices/like/likeSlice';
import { RootState } from '../../redux/store';

export interface LikeBarProps {
  mediaType: 'movie' | 'tv'
  id: number
  altImg: string
}

const LikeBar = ({ mediaType, id, altImg }: LikeBarProps) => {
  const [enableRequest, setEnableRequest] = useState(false);
  const dispatch = useDispatch();
  const likeState = useSelector((state: RootState) => state.like);
  const section = mediaType === 'movie' ? 'MOVIES' : 'TV_SHOWS';
  const isLiked = likeState[section].some(favId => favId === id);

  const {
    error
  } = useFetch<{ success: boolean, media_type?: string }>(
    {
      flag: enableRequest ? 'FETCH' : 'WAIT',
      api: genericAxiosInstance({
        baseUrl: process.env.NEXT_PUBLIC_FAVORITE_LOCAL_BASE_URL ?? ''
      }),
      requestConfig: {
        method: 'post',
        withCredentials: true,
        data: {
          media_type: mediaType,
          media_id: id,
          favorite: isLiked
        }
      }
    }
  );

  const toggleLike = () => {
    setEnableRequest(true);
    isLiked
      ? dispatch(setDisLike({ section, id }))
      : dispatch(setLike({ section, id }));
  };

  useEffect(() => {
    if (error)
      toggleLike();
  }, [error]);

  return (
    <div>
      <Image className={`${!isLiked ? 'invert contrast-100' : ''}`}
        src={like} height={32} width={32} alt={altImg} onClick={toggleLike}/>
    </div>
  );
};
export default LikeBar;
