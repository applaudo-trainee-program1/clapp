
import { useEffect, useState } from 'react';
import { DEFAULT_DEBOUNCE_TIMEOUT, MAX_TMDB_PAGE_ALLOWED } from '../../config';
import { calculateTotalPages } from '../../utils/utils';
import Image from 'next/image';
import { forwardArrow } from '../../public/images';
import { validateIntRange } from '../../utils/validations';
import { PaginationProps } from './types';

const Pagination = ({ setPage, page, resultCount, paginationTitle }:PaginationProps) => {
  const [pageWithoutDebounce, setPageWithoutDebounce] = useState<number>(page);
  const [inputPage, setInputPage] = useState<number | ''>('');
  const actualTotalPages = calculateTotalPages(resultCount);
  const totalPages = actualTotalPages>MAX_TMDB_PAGE_ALLOWED 
    ? MAX_TMDB_PAGE_ALLOWED 
    : actualTotalPages;

  const setPageFromInput = (e:React.ChangeEvent<HTMLInputElement>) => {
    const validatedInputValue = validateIntRange({
      max: totalPages,
      min: 1,
      value: e.target.value
    });
    if (validatedInputValue) {
      setInputPage(validatedInputValue);
      setPageWithoutDebounce(validatedInputValue);
    } else {
      setInputPage('');
      setPageWithoutDebounce(page);
    }
  };

  useEffect(() => {
    let timer: null | NodeJS.Timeout = null;
    if (page !== pageWithoutDebounce)
      timer = setTimeout(() => {
        setPage(pageWithoutDebounce);
      }, DEFAULT_DEBOUNCE_TIMEOUT);
    if (!pageWithoutDebounce && timer !== null) clearTimeout(timer);

    return () => { if (timer !== null) clearTimeout(timer); };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageWithoutDebounce]);
  
  return (
    <div className='flex justify-start items-center flex-col pt-4 gap-0 my-4'>
      <h3>{paginationTitle}&apos;s page</h3>
      <div className='flex justify-center items-center pt-4 gap-0 h-20'>
        <button
          className={pageWithoutDebounce === 1
            ? 'opacity-0 cursor-auto'
            : 'bg-transparent cursor-pointer rotate-180' }
          aria-label="previous page"
          onClick={() => {
            if (pageWithoutDebounce - 1 >= 1) setPageWithoutDebounce(prev => prev - 1);
          }}>
          <Image width={24} height={24} src={forwardArrow} alt="Previous page" />
        </button>
        <div className={totalPages === 1
          ? 'flex justify-center min-w-[15rem]'
          : 'flex justify-between min-w-[15rem]'}>
          {pageWithoutDebounce - 3 > 0 &&
          <>
            <button className={`flex justify-center items-center gap-4 bg-transparent 
            cursor-pointer select-none p-2 text-cyan-50`}
            onClick={ () => setPageWithoutDebounce(1)}>1</button>
            {pageWithoutDebounce - 3 !== 1 &&

            <span className="flex justify-center items-center gap-4 select-none text-cyan-50" >
              ...
            </span>}
          </> }
          {pageWithoutDebounce - 2 > 0 &&
        <button className={`flex justify-center items-center gap-4 bg-transparent 
            cursor-pointer select-none p-2 text-cyan-50`}
        onClick={ () => setPageWithoutDebounce(prev => prev - 2)}>
          {pageWithoutDebounce - 2}
        </button> }
          {pageWithoutDebounce - 1 > 0 &&
        <button className={`flex justify-center items-center gap-4 bg-transparent 
            cursor-pointer select-none p-2 text-cyan-50`}
        onClick={ () => setPageWithoutDebounce(prev => prev - 1)}>
          {pageWithoutDebounce - 1}
        </button> }
          <span className={`flex justify-center items-center gap-4 min-h-[2rem] min-w-[2rem] 
          rounded bg-cyan-50 text-black cursor-pointer select-none p-2`} aria-label="current page">
            {pageWithoutDebounce}
          </span>
          {pageWithoutDebounce + 1 <= totalPages &&
        <button className={`flex justify-center items-center gap-4 bg-transparent 
            cursor-pointer select-none p-2 text-cyan-50`}
        onClick={ () => setPageWithoutDebounce(prev => prev + 1)}>
          {pageWithoutDebounce + 1}
        </button> }
          {pageWithoutDebounce + 2 <= totalPages &&
        <button className={`flex justify-center items-center gap-4 bg-transparent 
            cursor-pointer select-none p-2 text-cyan-50`}
        onClick={ () => setPageWithoutDebounce(prev => prev + 2)}>
          {pageWithoutDebounce + 2}
        </button> }
          {pageWithoutDebounce + 3 <= totalPages &&
        <>{pageWithoutDebounce + 3 !== totalPages &&
        <span className="flex justify-center items-center gap-4 select-none text-cyan-50" >
          ...
        </span>}
        <button className={`flex justify-center items-center gap-4 bg-transparent 
            cursor-pointer select-none p-2 text-cyan-50`} aria-label='last page'
        onClick={ () => setPageWithoutDebounce(totalPages)}>
          {totalPages}
        </button>
        </> }
        </div>
        <button
          className={pageWithoutDebounce === totalPages
            ? 'opacity-0 cursor-auto'
            : 'bg-transparent cursor-pointer' }
          aria-label="next page"
          onClick={() => {
            if (pageWithoutDebounce + 1 <= totalPages) setPageWithoutDebounce(prev => prev + 1);
          }}>
          <Image width={24} height={24} src={forwardArrow} alt="Next page" />
        </button>
      </div>
      <div className="h-10">
        <input
          className="p-2 text-base rounded-lg text-black"
          type="text"
          placeholder='example: 2'
          onInput={setPageFromInput}
          value={inputPage}
          aria-label="set input page"
        />
      </div>
    </div>
  );
};

export default Pagination;
