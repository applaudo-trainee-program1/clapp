import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import RelatedSection from './RelatedSection';
import props from '../../mocks/props/relatedSection.json';
import { RelatedDataSection } from './types';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
      router: () => null
    };
  },
}));

describe('Test RelatedSection', () => {
  it('Should render a card', async () => {
    render(
      <RelatedSection 
        dataArray={props.dataArray as RelatedDataSection[]}
        title={props.title}
      />
    );
    const crew = screen.getByText(props.dataArray[0].title);
    const imageCrew = screen.getByAltText(props.dataArray[0].title);
    const jobCrew = screen.getByText(props.dataArray[0].job);
    expect(crew).toBeInTheDocument();
    expect(imageCrew).toBeInTheDocument();
    expect(jobCrew).toBeInTheDocument();
  });
});
