import Image from 'next/image';
import { useRouter } from 'next/router';
import { getScoreClassName } from '../../utils/utils';
import { HeaderDetailsProps } from './types';

const HeaderDetails = ({ title, image, score }:HeaderDetailsProps) => {
  const router = useRouter();

  return (
    <div className="flex flex-col my-16 gap-4">
      <div className="flex justify-start items-center flex-wrap gap-4">
        <h3 className="font-semibold text-2xl text-pink-600">{ title }</h3>
        { 
          score 
            ? <span 
              className={`flex justify-center items-center h-10 w-10 text-white font-semibold 
          rounded-full ${getScoreClassName(score)}`}
            >{ score }</span>
            : null
        }
      </div>
      <button 
        className='p-2 border-cyan-50 border-2 rounded w-fit self-end' 
        type="button" 
        onClick={() => router.back()}
      >
        Go back
      </button>
      <Image className='self-center w-auto' width={300} height={400} src={image} alt={title}/>
    </div>
  );
};
export default HeaderDetails;