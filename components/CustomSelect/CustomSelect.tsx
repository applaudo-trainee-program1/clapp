import Image from 'next/image';
import { useEffect, useState } from 'react';
import { checkMark } from '../../public/images';
import { motion } from 'framer-motion';
import { CustomSelectProps } from './types';

const CustomSelect = ({ title, optionsArray, selectionMode, handleUpdate }:CustomSelectProps) => {
  const [openSelect, setOpenSelect] = useState(false);
  const [values, setValues] = useState<Array<number | string>>([]);

  useEffect(() => {
    if(handleUpdate)
      handleUpdate(values);
  }, [values]);
  

  const setValue = (value: string | number) => {
    setValues( prev => {
      switch(selectionMode){
      case 'SINGLE': {
        const hasBeenClicked = prev.includes(value);
        return hasBeenClicked ? [] : [value];
      }
      case 'MULTIPLE': {
        const hasBeenClicked = prev.includes(value);
        const newSetValues = new Set(values);
        hasBeenClicked ? newSetValues.delete(value) : newSetValues.add(value);
        return Array.from(newSetValues);}
      }
    });
  };
  return (
    <div className={`relative min-h-[4rem] w-[10rem] ${openSelect ? 'z-20' : ''}`} 
      aria-label={`${selectionMode} ${title} custom select`}>
      <div 
        className={`w-[10rem] border-cyan-50 border-2 rounded my-4 cursor-pointer select-none 
        absolute z-10`} 
      >
        <h6 
          className='flex justify-center items-center bg-white-glass px-2 cursor-pointer' 
          onClick={ ()=>setOpenSelect(prev => !prev) }>
          { title }
        </h6>
        {
          openSelect ?
            <div>
              { optionsArray.map( (optionObject) => 
                <motion.div 
                  className={`flex justify-center items-center gap-2 px-2 border-dashed 
                  border-t-2 bg-black`}
                  key={optionObject.value} 
                  onClick={() =>setValue(optionObject.value)}
                  initial={{height: 0 }}
                  animate={{height: 'auto'}}
                >
                  <span>{optionObject.header}</span>
                  {
                    values.includes(optionObject.value) ? 
                      <Image 
                        src={checkMark} 
                        alt={`${optionObject.header} has been selected`} 
                        width={24} 
                        height={24}
                      /> 
                      : null
                  }
                </motion.div>)
              }
            </div>
            :
            null
        }
      </div>
    </div>
  );
};
export default CustomSelect;