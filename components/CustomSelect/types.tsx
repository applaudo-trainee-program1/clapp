/* eslint-disable no-unused-vars */
export type OptionObject = {
  header: string,
  value: number | string
}

export type CustomSelectProps = {
  title: string,
  optionsArray: OptionObject[],
  selectionMode: 'MULTIPLE' | 'SINGLE',
  handleUpdate: ({values}:Array<number | string>) => void
}
