import { screen, waitFor } from '@testing-library/dom';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import CustomSelect from './CustomSelect';
import customSelectProps from '../../mocks/props/customSelectMock.json';

describe('Test custom select interactivity', () => {
  it('Custom select was rendered successfully', async () => {
    const callbackTest = jest.fn();
    render(
      <CustomSelect
        title={customSelectProps.title}
        optionsArray={customSelectProps.optionsArray}
        selectionMode={customSelectProps.selectionMode as 'MULTIPLE'}
        handleUpdate={callbackTest}
      />
    );
    const title = screen.getByText(customSelectProps.title);
    expect(title).toBeInTheDocument();
  });
  
  it('Options should be rendering after title is clicked', async () => {
    window.scrollTo = jest.fn();
    const callbackTest = jest.fn();
    render(
      <CustomSelect
        title={customSelectProps.title}
        optionsArray={customSelectProps.optionsArray}
        selectionMode={customSelectProps.selectionMode as 'MULTIPLE'}
        handleUpdate={callbackTest}
      />
    );
    const title = screen.getByText(customSelectProps.title);
    userEvent.click(title);
    await waitFor(() => {
      const option = screen.getByText(customSelectProps.optionsArray[0].header);
      expect(option).toBeInTheDocument();
    });
  });

  it('Handler has been called', async () => {
    window.scrollTo = jest.fn();
    const callbackTest = jest.fn();
    render(
      <CustomSelect
        title={customSelectProps.title}
        optionsArray={customSelectProps.optionsArray}
        selectionMode={customSelectProps.selectionMode as 'MULTIPLE'}
        handleUpdate={callbackTest}
      />
    );
    const title = screen.getByText(customSelectProps.title);
    userEvent.click(title);
    await waitFor(()=>{
      const option = screen.getByText(customSelectProps.optionsArray[0].header);
      userEvent.click(option);
    });
    await waitFor(() => {
      expect(callbackTest).toBeCalled();
    });
  });
  
  it('Two options has been selected at same time', async () => {
    window.scrollTo = jest.fn();
    const callbackTest = jest.fn();
    render(
      <CustomSelect
        title={customSelectProps.title}
        optionsArray={customSelectProps.optionsArray}
        selectionMode={customSelectProps.selectionMode as 'MULTIPLE'}
        handleUpdate={callbackTest}
      />
    );
    const title = screen.getByText(customSelectProps.title);
    userEvent.click(title);
    const optionOne = await screen.findByText(customSelectProps.optionsArray[0].header);
    const optionTwo = await screen.findByText(customSelectProps.optionsArray[1].header);
    userEvent.click(optionOne);
    userEvent.click(optionTwo);
    await waitFor(async () => {
      const checkMarks = await screen.findAllByAltText(/has been selected/i);
      expect(checkMarks).toHaveLength(2);
    });
  });
  
  it('Only one has been selected at after click twice', async () => {
    window.scrollTo = jest.fn();
    const callbackTest = jest.fn();
    render(
      <CustomSelect
        title={customSelectProps.title}
        optionsArray={customSelectProps.optionsArray}
        selectionMode="SINGLE"
        handleUpdate={callbackTest}
      />
    );
    const title = screen.getByText(customSelectProps.title);
    userEvent.click(title);
    const optionOne = await screen.findByText(customSelectProps.optionsArray[0].header);
    const optionTwo = await screen.findByText(customSelectProps.optionsArray[1].header);
    userEvent.click(optionOne);
    userEvent.click(optionTwo);
    await waitFor(async () => {
      const checkMarks = await screen.findAllByAltText(/has been selected/i);
      expect(checkMarks).toHaveLength(1);
    });
  });
});
