import Head from 'next/head';
import { useRouter } from 'next/router';
import { useFetch } from '../../hooks';
import {  ITvShowDetails } from '../../ts/response';
import { Spinner, HeaderDetails, DescriptionSection, TagList, 
  RelatedSection, 
  ErrorCard} from '../../components';
import { TV_SHOW_DETAILS_LOCAL_BASE_URL } from '../../config';
import genericAxiosInstance from '../../apiInstances/genericApi';
import { normalizeTvShowDetails } from '../../utils/utils';

const TvShowDetails = () => {
  const router = useRouter();
  const { id } = router.query;
  const numberId = Number.isNaN(Number(id)) ? 0 : Number(id);
  const {
    loading, 
    response,
    error
  } = useFetch<ITvShowDetails>(
    { flag: router.isReady ? 'FETCH' : 'WAIT',
      api: genericAxiosInstance({baseUrl: TV_SHOW_DETAILS_LOCAL_BASE_URL || ''}),
      requestConfig:{
        ...(id ? {url: String(id)} : undefined) 
      }
    }
  );
  const normalizedData = (response && numberId)
    ? normalizeTvShowDetails({data: response, showId: numberId}) : null;

  return (
    <>
      <Head>
        <title>Clapp</title>
        <meta name="description" content="Clapp tv show details" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className='my-4'>
        {loading === false && normalizedData
          ? 
          <>
            <HeaderDetails title={normalizedData.title} image={normalizedData.img} 
              score={normalizedData.score}/>

            <DescriptionSection title={normalizedData.sectionTitle} 
              description={normalizedData.sectionContent}/>

            <TagList title="Genres" list={normalizedData.genders?.map(gender => gender.name)}/>
            
            <RelatedSection title='Seasons' dataArray={normalizedData.seasons}/>

            <RelatedSection title='Cast' dataArray={normalizedData.persons}/>

            <RelatedSection title='Crew' dataArray={normalizedData.crewPersons}/>

            <span className='text-lg font-bold my-4'>
              Release date: { normalizedData.releaseDate }
            </span>
          </>
          : null
        }
        { loading === true 
          ? 
          <div className='flex justify-center items-center min-h-[4rem] my-16'>
            <Spinner />
          </div>
          : null
        }
        { loading === false && error 
          ? <ErrorCard />
          : null
        }
      </main>
    </>
  );
};
export default TvShowDetails;