import Head from 'next/head';
import { genericApi } from '../apiInstances';
import { GridLayout, Spinner } from '../components';
import { useFetch } from '../hooks';
import { IResponseDiscoverMovies, IResponseDiscoverShows } from '../ts/response';
import { normalizeLocalResponse } from '../utils/utils';

const Profile = () => {
  const api = genericApi({ baseUrl: process.env.NEXT_PUBLIC_PROFILE_LOCAL_BASE_URL ?? '' });
  const {
    loading,
    response
  } = useFetch<{ movies: IResponseDiscoverMovies, tv: IResponseDiscoverShows }>({
    api,
    requestConfig: {
      withCredentials: true
    }
  });

  const normalizedMoviesResponse = normalizeLocalResponse({
    results: response?.movies.results,
    section: 'MOVIES'
  });

  const normalizedTvShowsResponse = normalizeLocalResponse({
    results: response?.tv.results,
    section: 'TV_SHOWS'
  });

  return (
    <>
      <Head>
        <title>Clapp - Home</title>
        <meta name="description" content="Clapp - resource not found" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main >
        {!loading && (normalizedMoviesResponse != null) && (normalizedTvShowsResponse != null)

          ? <>
            <GridLayout
              loading={loading}
              dataArray={normalizedMoviesResponse}
              titleLayout="Favorite movies"
              section = "BOTH"
            />
            <GridLayout
              loading={loading}
              dataArray={normalizedTvShowsResponse}
              titleLayout="Favorite tv shows"
              section = "BOTH"
            />
          </>
          : <div className='flex justify-center items-center min-h-[20rem]'><Spinner/></div>
        }
      </main>
    </>
  );
};
export default Profile;
