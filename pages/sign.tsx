/* eslint-disable @typescript-eslint/naming-convention */
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { genericApi } from '../apiInstances';
import cookie from 'cookie';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setSignIn } from '../redux/slices/sign/signSlice';
import { Routes } from '../routes/routes';
import { RootState } from '../redux/store';
import { ILocalRequestTokenResponse, ILocalSessionResponse } from '../ts/response';
import { getAuthUrl } from '../utils/utils';

interface Props {
  requestToken?: string
  isLogged?: boolean
  error?: string | boolean
}

const Sign: NextPage<Props> = ({ requestToken, isLogged, error }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const signState = useSelector((state: RootState) => state.sign);

  useEffect(() => {
    if (error)
      void router.replace('/sign', undefined, { shallow: true });
  }, [error]);

  useEffect(() => {
    if (isLogged || signState.isLogged) {
      dispatch(setSignIn());
      router.push(Routes.HOME);
    }
  }, [isLogged, signState.isLogged]);

  return (
    <div className='flex justify-center items-center h-40'>
      {
        isLogged !== true
          ? <a className='bg-white-glass p-2 rounded-md'
            href={getAuthUrl(String(requestToken))}
            target="_blank" rel="noopener noreferrer">
            Authorize
          </a>
          : <span>already login</span>
      }
    </div>
  );
};
export default Sign;

Sign.getInitialProps = async ({ req, res, query: { request_token, approved } }) => {
  try {
    const cookies = cookie.parse((req != null) ? req.headers.cookie ?? '' : document.cookie);
    const sessionId = cookies.sessionId;
    if (sessionId !== undefined)
      return {
        isLogged: true
      };
    const state = (request_token !== undefined && approved !== undefined) 
      ? 'REQUEST_NEW_STEP_2' 
      : 'REQUEST_NEW_STEP_1';
    switch (state) {
    case 'REQUEST_NEW_STEP_1':
    {
      try {
        const api = genericApi({
          baseUrl: process.env
            .NEXT_PUBLIC_REQUEST_TOKEN_LOCAL_BASE_URL ?? ''
        });
        const response = await api<ILocalRequestTokenResponse>({ method: 'get' });
        const requestToken = response.data.requestToken;
        return {
          isLogged: false,
          error: false,
          requestToken
        };
      } catch (error) {
        return {
          isLogged: false,
          error: 'Error step 1'
        };
      }
    }
    case 'REQUEST_NEW_STEP_2':{
      try {
        const sessionApi = genericApi({
          baseUrl: `${process.env
            .NEXT_PUBLIC_SIGN_LOCAL_BASE_URL ?? ''}?requestToken=${request_token}`
        });
        const response = await sessionApi<ILocalSessionResponse>({
          method: 'get'
        });
        res?.setHeader(
          'Set-Cookie',
          cookie.serialize('sessionId',
            JSON.stringify({
              sessionId: response.data.sessionId,
              accountId: response.data.accountId,
              accountName: response.data.accountName
            })
            , {
              httpOnly: true,
              secure: true,
              maxAge: 50 * 60 * 1000,
              sameSite: 'strict',
              path: '/'
            })
        );
        return {
          isLogged: true,
          error: false
        };
      } catch (error) {
        res?.setHeader(
          'Set-Cookie',
          cookie.serialize('sessionId', '', {
            httpOnly: true,
            secure: true,
            maxAge: 50 * 60 * 1000,
            sameSite: 'strict',
            path: '/'
          })
        );
        return {
          isLogged: false,
          error: 'Error step 2'
        };
      }
    }
    default:
      return {
        isLogged: false
      };
    }
  } catch (error) {
    return {
      isLogged: false,
      error: 'Error on server'
    };
  }
};
