import Head from 'next/head';
import { ErrorCard } from '../components';

const NotFound = () => {
  return (
    
    <>
      <Head>
        <title>Clapp - Home</title>
        <meta name="description" content="Clapp - resource not found" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main >
        <ErrorCard />
      </main>
    </>
  );
};
export default NotFound;