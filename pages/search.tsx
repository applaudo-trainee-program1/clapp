import Head from 'next/head';
import { useRef, useState } from 'react';
import { GridLayout, InputDebounce, Pagination } from '../components';
import { useFetch } from '../hooks';
import { IResponseDiscoverMovies, IResponseDiscoverShows, IResponsePeople } from '../ts/response';
import { getLocalSearchRequestConfig, normalizeLocalSearchResponse  } from '../utils/utils';

const Search = () => {
  const [page, setPage] = useState(1);
  const [query, setQuery] = useState('');
  const scrollRef = useRef<HTMLDivElement>(null);  
  const {
    loading, 
    response, 
    refetch
  } = useFetch<IResponseDiscoverMovies | IResponseDiscoverShows | IResponsePeople>(
    getLocalSearchRequestConfig({page, query})
  );

  const normalizedResponse = normalizeLocalSearchResponse({
    results: response?.results
  });

  const scrollTo = () => {
    if (scrollRef && scrollRef.current ) 
      scrollRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
    
  };

  const handleSetNewPage = (newPage:number) => {
    setPage(newPage);
    refetch();
    scrollTo();
  };

  const handleSetNewSearch = (newSearch:string) => {
    setPage(1);
    setQuery(newSearch);
    refetch();
    scrollTo();
  };

  return (
    <>
      <Head>
        <title>Clapp - Movies & tv shows search page</title>
        <meta name="description" content="Clapp - Movies & TV shows search page" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main >
        <><div ref={scrollRef}/>
          <InputDebounce type='TEXT' initialValue='' title='Search' 
            containerclassName='flex items-center justify-center gap-4 mt-8'
            inputClassname='p-2 text-base rounded-lg text-black w-60 text-center'
            handleUpdate={handleSetNewSearch}/>
          <GridLayout
            loading={loading} 
            dataArray={normalizedResponse} 
            titleLayout="Movies & TV shows"
            section = "BOTH"
          />
          {loading === false && response && normalizedResponse?.length
            ? <Pagination page={page} setPage={handleSetNewPage}
              resultCount={response.total_results} paginationTitle="Search"/>
            : null
          }
        </>
      </main>
    </>
  );
};
export default Search;