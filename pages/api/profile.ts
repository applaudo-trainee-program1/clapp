import { NextApiRequest, NextApiResponse } from 'next';
import { genericApi } from '../../apiInstances';
import { enviromentVariables } from '../../utils';
import cookie from 'cookie';

const { apiKey } = enviromentVariables;
const api = genericApi({ baseUrl: process.env.NEXT_PUBLIC_PROFILE_BASE_URL ?? '' });
api.defaults.params = {};
api.interceptors.request.use(function (config) {
  config.params.api_key = apiKey;
  return config;
}, async function (error) {
  return await Promise.reject(error);
});

export interface IRequestTokenResponse {
  success: boolean
  expiresAt: string
  requestToken: string
}

const profile = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { accountId, sessionId } = req.query;
    if (Array.isArray(accountId) || Array.isArray(sessionId)) 
    { res.status(400).json({ success: false }); return; }
    const cookies = cookie.parse(req.headers.cookie ?? '');
    const account = cookies?.sessionId;
    const accountObject: { accountId: string, sessionId: string } = account !== undefined
      ? JSON.parse(account)
      : undefined;
    const user = accountId ?? accountObject?.accountId;
    const session = sessionId ?? accountObject?.sessionId;
    const moviesResponse = await api({
      baseURL: process.env.NEXT_PUBLIC_PROFILE_BASE_URL,
      method: 'get',
      url: `${user}/favorite/movies`,
      params: {
        session_id: session
      }
    });
    const tvShowsResponse = await api({
      baseURL: process.env.NEXT_PUBLIC_PROFILE_BASE_URL,
      method: 'get',
      url: `${user}/favorite/tv`,
      params: {
        session_id: session
      }
    });
    res.status(200).json({ movies: moviesResponse.data, tv: tvShowsResponse.data });
  } catch (error) {
    res.status(400).json({ success: false });
  }
};

export default profile;
