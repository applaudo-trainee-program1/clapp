import { NextApiRequest, NextApiResponse } from 'next';
import { genericApi } from '../../apiInstances';
import { enviromentVariables } from '../../utils';
import { camelizeKeys, decamelizeKeys } from 'humps';
import { AxiosResponse } from 'axios';

const { requestTokenBaseUrl, apiKey } = enviromentVariables;
const api = genericApi({baseUrl: requestTokenBaseUrl || ''});
api.defaults.params = {};
api.interceptors.request.use(function (config) {
  config.params['api_key'] = apiKey;
  return config;
}, function (error) {
  return Promise.reject(error);
});


api.interceptors.response.use((response: AxiosResponse) => {
  if (response.data ) 
    response.data = camelizeKeys(response.data);
  return response;
});
    
api.interceptors.request.use((config) => {
  const newConfig = { ...config };

  if (config.params) 
    newConfig.params = decamelizeKeys(config.params);
  
  if (config.data) 
    newConfig.data = decamelizeKeys(config.data);
  

  return newConfig;
});

export interface IRequestTokenResponse {
  success: boolean, 
  expiresAt: string, 
  requestToken: string
}

const requestToken = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const response = await api.get<IRequestTokenResponse>(requestTokenBaseUrl || '');
    res.status(200).json({ requestToken: response.data.requestToken });
  } catch (error) {
    res.status(400).json({ success: false });
  }
};

export default requestToken;