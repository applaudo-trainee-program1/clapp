import { AxiosResponse } from 'axios';
import { NextApiRequest, NextApiResponse } from 'next';
import { genericApi } from '../../apiInstances';
import cookie from 'cookie';
import { camelizeKeys, decamelizeKeys } from 'humps';
import { enviromentVariables } from '../../utils';

const { apiKey } = enviromentVariables;
const api = genericApi({baseUrl: process.env
  .NEXT_PUBLIC_FAVORITE_BASE_URL || ''});

api.defaults.params = {};
api.interceptors.request.use(function (config) {
  config.params['api_key'] = apiKey;
  return config;
}, function (error) {
  return Promise.reject(error);
});

api.interceptors.response.use((response: AxiosResponse) => {
  if (response.data ) 
    response.data = camelizeKeys(response.data);
  return response;
});
    
api.interceptors.request.use((config) => {
  const newConfig = { ...config };

  if (config.params) 
    newConfig.params = decamelizeKeys(config.params);
  
  if (config.data) 
    newConfig.data = decamelizeKeys(config.data);

  return newConfig;
});


const requestToken = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { media_type, media_id, favorite } = req.body;
    const cookies = cookie.parse(req.headers.cookie || '');
    const account = cookies.sessionId;
    const accountObject = JSON.parse(account);
    const response = await api({
      method:'post',
      url: `${accountObject?.accountId}/favorite`,
      params:{
        sessionId: accountObject?.sessionId
      },
      data:{ mediaType:media_type, mediaId:media_id, favorite }
    });
    res.status(200).json({ requestToken: response.data.requestToken });
  } catch (error) {
    res.status(400).json({ success: false });
  }
};

export default requestToken;