import {  PERSON_DETAILS_BASE_URL } from './../../../../config/env';
import { NextApiRequest, NextApiResponse } from 'next';
import { getGenericDetailsRequestConfig } from '../../../../utils/utils';
import { isValidCastNumber } from '../../../../utils/validations';

const movieDetails = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { id } = req.query;
    const validatedId = isValidCastNumber({toValidate: id});
    if(!validatedId) res.status(400).json({message: 'No id where provided'});
    const { requestConfig, api } = getGenericDetailsRequestConfig({
      baseUrl: PERSON_DETAILS_BASE_URL || '',
      ...(validatedId ? { id: String(validatedId) } : undefined )
    });
    const response = await api({
      ...requestConfig
    });
    res.status(200).json(response.data);
  } catch (error) {
    res.status(404).json({message: 'Resource not found'});
  }
};

export default movieDetails;