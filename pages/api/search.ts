import { MIN_PAGE_REQUEST } from './../../config/config';
import { NextApiRequest, NextApiResponse } from 'next';
import api from '../../apiInstances/searchMoviesTvShowsApi';
import { enviromentVariables } from '../../utils';
import { isValidCastNumber } from '../../utils/validations';

const searchMoviesTvShows = async (req: NextApiRequest, res: NextApiResponse) => {
  const { page, query } = req.query;
  const { apiKey } = enviromentVariables;
  const validatedPage = isValidCastNumber({ toValidate: page });
  const requestConfig = {
    method: 'get',
    params: {
      api_key: apiKey,
      language: 'en-US',
      query: query ?? 'disney',
      include_adult: false,
      page: validatedPage >= MIN_PAGE_REQUEST ? validatedPage : MIN_PAGE_REQUEST
    }
  };
  const response = await api({
    ...requestConfig
  });
  res.status(200).json(response.data);
};

export default searchMoviesTvShows;
