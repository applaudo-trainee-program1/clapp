import { NextApiRequest, NextApiResponse } from 'next';
import { MIN_PAGE_REQUEST } from '../../../config';
import { getMainShowsRequestConfig } from '../../../utils/utils';
import { 
  isValidCastArrayOfNumber, isValidCastNumber 
} from '../../../utils/validations';

const mainShowList = async (req: NextApiRequest, res: NextApiResponse) => {
  const { page, genres, year } = req.query;
  const validatedPage = isValidCastNumber({toValidate: page});
  const validatedYear = isValidCastNumber({toValidate: year});
  const validatedGenres = isValidCastArrayOfNumber({toValidate: genres});
  const { requestConfig, api } = getMainShowsRequestConfig({
    section: 'TV_SHOWS',
    ...(validatedPage ? 
      {page: validatedPage >= MIN_PAGE_REQUEST ? validatedPage : MIN_PAGE_REQUEST} 
      : undefined),
    ...(validatedYear ? {year: validatedYear} : undefined),
    ...(validatedGenres ? {genres: validatedGenres} : undefined),
  });
  const response = await api({
    ...requestConfig
  });
  res.status(200).json(response.data);
};

export default mainShowList;