import { NextApiRequest, NextApiResponse } from 'next';
import { MIN_PAGE_REQUEST } from '../../../config';
import { ValidMoviesCertifications } from '../../../utils/types';
import { getMainMoviesRequestConfig } from '../../../utils/utils';
import { 
  isValidCastArrayOfNumber, isValidCastNumber, isValidMovieCertification 
} from '../../../utils/validations';

const mainMovieList = async (req: NextApiRequest, res: NextApiResponse) => {
  const { page, genres, year, certification } = req.query;
  const validatedPage = isValidCastNumber({toValidate: page});
  const validatedYear = isValidCastNumber({toValidate: year});
  const validatedGenres = isValidCastArrayOfNumber({toValidate: genres});
  const isValidCertification = isValidMovieCertification({toValidate: certification});
  const { requestConfig, api } = getMainMoviesRequestConfig({
    section: 'MOVIES',
    ...(validatedPage ? 
      {page: validatedPage >= MIN_PAGE_REQUEST ? validatedPage : MIN_PAGE_REQUEST} 
      : undefined),
    ...(validatedYear ? {year: validatedYear} : undefined),
    ...(validatedGenres ? {genres: validatedGenres} : undefined),
    ...(isValidCertification 
      ? {certification: certification as ValidMoviesCertifications} 
      : undefined),
  });
  const response = await api({
    ...requestConfig
  });
  res.status(200).json(response.data);
};

export default mainMovieList;