import { GetServerSideProps } from 'next';
import Head from 'next/head';
import ListSection from '../components/ListSection/ListSection';
import { IResponseDiscoverMovies, IResponseDiscoverShows } from '../ts/response';
import cookie from 'cookie';
import { genericApi } from '../apiInstances';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setLike } from '../redux/slices/like/likeSlice';
export interface HomeProps {
  likes?: { movies: IResponseDiscoverMovies, tv: IResponseDiscoverShows }
}

const Home = ({ likes }: HomeProps) => {
  const dispatch = useDispatch();
  useEffect(() => {
    if (likes !== undefined && (likes.movies.results.length > 0))
      likes.movies.results.forEach(movie =>
        dispatch(setLike({ section: 'MOVIES', id: movie.id })));

    if (likes !== undefined && (likes.tv.results.length > 0))
      likes.movies.results.forEach(show =>
        dispatch(setLike({ section: 'TV_SHOWS', id: show.id })));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [likes]);

  return (
    <>
      <Head>
        <title>Clapp - Home</title>
        <meta name="description" content="Clapp - home page" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main >
        <ListSection section="MOVIES" sectionTitle='Movies' paginationTitle="Movie"/>
        <ListSection section="TV_SHOWS" sectionTitle='TV shows' paginationTitle="Show"/>
      </main>
    </>
  );
};

export const getServerSideProps: GetServerSideProps<HomeProps> = async (
  { req }
) => {
  const cookies = cookie.parse(req.headers.cookie ?? '');
  const account = cookies.sessionId;
  const accountObject: { accountId: string | undefined, sessionId: string | undefined } 
  = account !== undefined
    ? JSON.parse(account)
    : undefined;
  if (accountObject?.sessionId === undefined || accountObject?.accountId === undefined)
    return {
      props: {
      }
    };

  const api = genericApi({ baseUrl: process.env.NEXT_PUBLIC_PROFILE_LOCAL_BASE_URL ?? '' });
  const likesResponse = await api<HomeProps['likes']>({
    method: 'get',
    params: {
      sessionId: accountObject.sessionId,
      accountId: accountObject.accountId
    }
  });
  return {
    props: {
      likes: likesResponse.data
    }
  };
};
export default Home;
