import Head from 'next/head';
import { useRouter } from 'next/router';
import { useFetch } from '../../hooks';
import {  IPersonDetails } from '../../ts/response';
import { Spinner, HeaderDetails, DescriptionSection, ErrorCard} from '../../components';
import { getPersonDetailsRequestConfig, normalizePersonDetails } from '../../utils/utils';

const PersonDetails = () => {
  const router = useRouter();
  const { id } = router.query;
  const numberId = Number.isNaN(Number(id)) ? 0 : Number(id);
  const {
    loading, 
    response,
    error
  } = useFetch<IPersonDetails>(
    getPersonDetailsRequestConfig({flag: router.isReady, id:numberId}));
  const normalizedData = (response && numberId)
    ? normalizePersonDetails({data: response}) : null;

  return (
    <>
      <Head>
        <title>Clapp</title>
        <meta name="description" content="Clapp - person details" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className='my-4'>
        {loading === false && normalizedData
          ? 
          <>
            <HeaderDetails title={normalizedData.title} image={normalizedData.img} />

            <DescriptionSection title={normalizedData.sectionTitle} 
              description={normalizedData.sectionContent}/>
            
            <p className='text-lg font-bold my-4'>
              Birthday: { normalizedData.birthday || 'Unknown birthday' }
            </p>

            {
              normalizedData.deathday 
                ? 
                <p className='text-lg font-bold my-4'>
                Deathday: { normalizedData.deathday }
                </p>
                : null
            }

            <p className='text-lg font-bold my-4'>
              Born in: { normalizedData.placeOfBirth || 'Unknown place of birth' }
            </p>
          </>
          : null
        }
        { loading === true 
          ? 
          <div className='flex justify-center items-center min-h-[4rem] my-16'>
            <Spinner />
          </div>
          : null
        }
        { loading === false && error 
          ? <ErrorCard />
          : null
        }
      </main>
    </>
  );
};
export default PersonDetails;