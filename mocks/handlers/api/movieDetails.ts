import { rest } from 'msw';
import movieDetails from '../../responses/movieDetails.json';

export const movieDetailsHandler = [
  rest.get('/api/details/movies/809', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(movieDetails));
    
  }),
];
