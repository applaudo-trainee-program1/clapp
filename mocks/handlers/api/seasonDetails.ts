import { rest } from 'msw';
import seasonDetails from '../../responses/seasonDetails.json';

export const seasonDetailsHandler = [
  rest.get('/api/details/season/70523', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(seasonDetails));
  }),
];
