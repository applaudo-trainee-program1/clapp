import { rest } from 'msw';
import mainMovies from '../../responses/mainMovies.json';

export const mainMoviesHandler = [
  rest.get('/api/main/movies', (req, res, ctx) => {
    const page = req.url.searchParams.get('page');
    if(page === '2')  
      return res(ctx.status(400), ctx.json({error: 'resource not found'}));
    
    return res(ctx.status(200), ctx.json(mainMovies));
    
  }),
];
