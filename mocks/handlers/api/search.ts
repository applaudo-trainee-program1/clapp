import { rest } from 'msw';
import searchResponse from '../../responses/search.json';
import searchShrekResponse from '../../responses/searchShrek.json';

export const searchHandler = [
  rest.get('/api/search', (req, res, ctx) => {
    const page = req.url.searchParams.get('page');
    const query = req.url.searchParams.get('query');
    if(query === 'shrek')  
      return res(ctx.status(200), ctx.json(searchShrekResponse));
    if(page === '2')  
      return res(ctx.status(400), ctx.json({error: 'resource not found'}));
    
    return res(ctx.status(200), ctx.json(searchResponse));
    
  }),
];
