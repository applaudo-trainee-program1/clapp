const nextJest = require('next/jest');

const createJestConfig = nextJest({ dir: '.' });

const customJestConfig = {
  testEnvironment: 'jsdom',
  clearMocks: true,
  moduleDirectories: ['node_modules', 'components', 'pages'],
  setupFilesAfterEnv: ['<rootDir>/setupTests.ts'],
  testRegex: '(/__tests__/.*|(\\.|/)test)\\.[jt]sx?$',
  collectCoverageFrom: [
    'components/**/*.{tsx,jxs}',
    'hooks/**/*.{tsx,jxs}',
    'utils/**/*.{tsx,jxs,ts}',
    'redux/**/*.{tsx,jxs,ts}',
    '!redux/enums/*.{tsx,jxs,ts}',
    'pages/**/*.{tsx,jxs,ts}',
    '!pages/api/**/*.{tsx,jxs,ts}',
    '!pages/_app.tsx',
    '!pages/_document.tsx',
    '!pages/sign.tsx',
    '!**/types.ts'
  ]
};

module.exports = createJestConfig(customJestConfig);
