/* eslint-disable no-unused-vars */
export enum FilterActionType {
  RESET_ALL = 'RESET_ALL',
  SET_CERTIFICATION = 'SET_CERTIFICATION',
  SET_GENRES = 'SET_GENRES',
  SET_YEAR = 'SET_YEAR'
}