import {store} from '../redux/store';
import { initialFilterState } from '../config/initials';
import { render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import FilterSliceTest from '../testUtils/FilterSliceTest';
import userEvent from '@testing-library/user-event';
import props from '../mocks/props/filterSliceTest.json';

describe('Test filter redux reducer', () => {
  it('Should initializes as initial filter state', () => {
    const state = store.getState().filter;
    expect(state).toEqual(initialFilterState);
  });

  it('Should update movie year', async () => {
    render(
      <Provider store={store}>
        <FilterSliceTest newState={props.newState}/>
      </Provider>
    );
    const updateMovieYear = screen.getByText('Update movie year');
    userEvent.click(updateMovieYear);
    await waitFor(() => {
      const currentMovieYear = screen.getByLabelText('currentMovieYear');
      expect(currentMovieYear).toHaveTextContent(String(props.newState['MOVIES'].year));
    });
  });
  
  it('Should tv show year', async () => {
    render(
      <Provider store={store}>
        <FilterSliceTest newState={props.newState}/>
      </Provider>
    );
    const updateShowYear = screen.getByText('Update show year');
    userEvent.click(updateShowYear);
    await waitFor(() => {
      const currentShowYear = screen.getByLabelText('currentShowYear');
      expect(currentShowYear).toHaveTextContent(String(props.newState['TV_SHOWS'].year));
    });
  });

  it('Should update movie genres', async () => {
    render(
      <Provider store={store}>
        <FilterSliceTest newState={props.newState}/>
      </Provider>
    );
    const updateMovieGenres = screen.getByText('Update movie genres');
    userEvent.click(updateMovieGenres);
    await waitFor(() => {
      const currentMovieGenres = screen.getByLabelText('currentMovieGenres');
      expect(currentMovieGenres)
        .toHaveTextContent(JSON.stringify(props.newState['MOVIES'].genres));
    });
  });

  it('Should update show genres', async () => {
    render(
      <Provider store={store}>
        <FilterSliceTest newState={props.newState}/>
      </Provider>
    );
    const updateShowGenres = screen.getByText('Update show genres');
    userEvent.click(updateShowGenres);
    await waitFor(() => {
      const currentShowGenres = screen.getByLabelText('currentShowGenres');
      expect(currentShowGenres)
        .toHaveTextContent(JSON.stringify(props.newState['TV_SHOWS'].genres));
    });
  });

  it('Should update movie certification', async () => {
    render(
      <Provider store={store}>
        <FilterSliceTest newState={props.newState}/>
      </Provider>
    );
    const updateMovieCertification = screen.getByText('Update movie certification');
    userEvent.click(updateMovieCertification);
    await waitFor(() => {
      const currentMovieCertification = screen.getByLabelText('currentMovieCertification');
      expect(currentMovieCertification)
        .toHaveTextContent(JSON.stringify(props.newState['MOVIES'].certification));
    });
  });
});