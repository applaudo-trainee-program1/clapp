import { configureStore } from '@reduxjs/toolkit';
import filterReducer from './slices/filter/filterSlice';
import signReducer from './slices/sign/signSlice';
import likeReducer from './slices/like/likeSlice';
import storage from 'redux-persist/lib/storage';
import { combineReducers } from '@reduxjs/toolkit';
import { persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';

const persistConfig = {
  key: 'clapp',
  storage,
  blacklist: ['filter'],
};

const rootReducer = combineReducers({
  filter: filterReducer,
  sign: signReducer,
  like: likeReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: [thunk]
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
