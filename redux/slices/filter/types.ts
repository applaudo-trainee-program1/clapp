import { FilterActionType } from '../../enums/filter';

export type Certification = string[] | null
export type Genres = number[] | null
export type Year = number | null

export type SectionState = {
  certification: Certification,
  genres: Genres,
  year: Year
}

export type FilterState = Record<'MOVIES' | 'TV_SHOWS', SectionState>

/* --------------------------------- ACTIONS -------------------------------- */

export type ResetFilterAction = { type: FilterActionType.RESET_ALL }

export type SetCertificationFilterAction = { 
    payload: { certification: Certification, section: 'MOVIES' }, 
    type: FilterActionType.SET_CERTIFICATION 
  }
  
export type SetGenresFilterAction = { 
    payload: { genres: Genres, section: 'MOVIES' | 'TV_SHOWS' }, 
    type: FilterActionType.SET_GENRES 
  }
  
export type SetYearFilterAction = { 
    payload: { year: Year, section: 'MOVIES' | 'TV_SHOWS' }, 
    type: FilterActionType.SET_YEAR 
  }
  

export type SetFilterCertificationProps = SetCertificationFilterAction['payload']

export type SetFilterGenreProps = SetGenresFilterAction['payload']

export type SetFilterYearProps = SetYearFilterAction['payload']