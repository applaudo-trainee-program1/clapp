import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import { initialFilterState } from '../../../config/initials';
import { SetCertificationFilterAction, SetGenresFilterAction, SetYearFilterAction } from './types';

export const filterSlice = createSlice({
  name: 'filter',
  initialState: initialFilterState,
  reducers: {
    resetAll: () => { return initialFilterState;},
    setCertification: (state, action: PayloadAction<SetCertificationFilterAction['payload']>) => {
      const { payload } = action;
      const newState = { ...state, 'MOVIES':{
        ...state['MOVIES'],
        certification: payload.certification?.length ? payload.certification : null
      }};
      return newState;
      
    },
    setGenres: (state, action: PayloadAction<SetGenresFilterAction['payload']>) => {
      const { payload } = action;
      const newState = { 
        ...state, 
        [payload.section]:{
          ...state[payload.section],
          genres: payload.genres?.length ? payload.genres : null
        }};
      return newState;
    },
    setYear: (state, action: PayloadAction<SetYearFilterAction['payload']>) => {
      const { payload } = action;
      const newState = { 
        ...state, 
        [payload.section]:{
          ...state[payload.section],
          year: payload.year ? payload.year : null
        }};
      return newState;
    },
  }
});

// Action creators are generated for each case reducer function
export const { resetAll, setCertification, setGenres, setYear } = filterSlice.actions;

export default filterSlice.reducer;
