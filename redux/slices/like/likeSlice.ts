import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import { initialLikeState } from '../../../config/initials';
import { SetLike, SetDislike, LikeState } from './types';


export const likeSlice = createSlice({
  name: 'like',
  initialState: initialLikeState,
  reducers: {
    setLike: (state, action: PayloadAction<SetLike['payload']>):LikeState => {
      const { payload } = action;
      const newState = { 
        ...state, 
        [payload.section]:[
          payload.id,
          ...state[payload.section],
        ]};
      return newState;
    },
    setDisLike: (state, action: PayloadAction<SetDislike['payload']>):LikeState => {
      const { payload } = action;
      const newState = { 
        ...state, 
        [payload.section]: state[payload.section].filter( favId => favId !== payload.id)
      };
      return newState;
    },
  }
});

// Action creators are generated for each case reducer function
export const { setLike, setDisLike } = likeSlice.actions;

export default likeSlice.reducer;
