/* eslint-disable no-unused-vars */
export enum Routes {
  HOME = '/',
  MOVIES = '/movies',
  TV_SHOWS = '/tv-shows',
  SIGN = '/sign',
  SEARCH = '/search',
  SEASON = '/season',
  PERSON = '/persons',
  PROFILE = '/profile'
}
