import { AxiosResponse } from 'axios'
import { useState, useEffect } from 'react'
import { RequestConfig, useFetchParams } from './types'

const useFetch = <Data, >({
  api,
  requestConfig,
  flag = 'FETCH'
}: useFetchParams) => {
  const [response, setResponse] = useState<Data | null>(null)
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(true)
  const [reload, setReload] = useState(false)

  const refetch = () => {
    setResponse(null)
    setError('')
    setLoading(true)
    setReload(prev => !prev)
  }

  useEffect(() => {
    const controller = new AbortController()

    const fetchData = async () => {
      try {
        const res = await api<Data, AxiosResponse<Data, RequestConfig>,
        Record<string, string | number | boolean>>({
          ...requestConfig,
          signal: controller.signal
        })
        setResponse(res.data)
        setLoading(false)
      } catch (err) {
        const guardError = err instanceof Error ? err.message : String(err)
        if (!guardError.includes('cancel') && !guardError.includes('abort')) {
          setError(guardError)
          setLoading(false)
        }
      }
    }

    if (flag === 'FETCH') fetchData()

    return () => {
      controller.abort()
      setResponse(null)
      setError('')
      setLoading(true)
    }
  }, [reload, flag])

  return { error, loading, refetch, response }
}

export default useFetch
