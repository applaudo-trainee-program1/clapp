import { screen, waitFor } from '@testing-library/dom';
import { act, render } from '@testing-library/react';
import PageProvider from '../testUtils/PageProvider';
import ShowDetails from '../pages/tv-shows/[id]';
import showDetails from '../mocks/responses/showDetails.json';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
      query: { id: 85937 },
      isReady: true
    };
  },
}));

describe('Test show details page', () => {
  it('Should render tv show details section & card with data fetched', async () => {
    await act(() => {
      render(
        <PageProvider>
          <ShowDetails />
        </PageProvider>
      );
    });
    await waitFor(() => {
      const title = screen.getByText(showDetails.name);
      const posterImg = screen.getByAltText(showDetails.name);
      expect(title).toBeInTheDocument();
      expect(posterImg).toBeInTheDocument();
    });
  });
});