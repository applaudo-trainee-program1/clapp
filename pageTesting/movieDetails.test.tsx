import { screen, waitFor } from '@testing-library/dom';
import { act, render } from '@testing-library/react';
import PageProvider from '../testUtils/PageProvider';
import MovieDetails from '../pages/movies/[id]';
import movieDetails from '../mocks/responses/movieDetails.json';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
      query: { id: 809 },
      isReady: true
    };
  },
}));

describe('Test movie details page', () => {
  it('Should render movie details section & card with data fetched', async () => {
    await act(() => {
      render(
        <PageProvider>
          <MovieDetails />
        </PageProvider>
      );
    });
    await waitFor(() => {
      const title = screen.getByText(movieDetails.title);
      const posterImg = screen.getByAltText(movieDetails.title);
      expect(title).toBeInTheDocument();
      expect(posterImg).toBeInTheDocument();
    });
  });
});