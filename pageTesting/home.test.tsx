import { screen, waitFor } from '@testing-library/dom';
import { act, render } from '@testing-library/react';
import PageProvider from '../testUtils/PageProvider';
import Home from '../pages/index';
import mainMovies from '../mocks/responses/mainMovies.json';
import mainShows from '../mocks/responses/mainShows.json';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
    };
  },
}));

describe('Test home page', () => {
  it('Should render movie section & card with data fetched', async () => {
    await act(() => {
      render(
        <PageProvider>
          <Home />
        </PageProvider>
      );
    });
    const sectionTitle = screen.getByText(/movies/i);
    expect(sectionTitle).toBeInTheDocument();
    await waitFor(() => {
      const cardTitle = screen.getByText(mainMovies.results[0].title);
      const cardImg = screen.getByAltText(mainMovies.results[0].title);
      expect(cardTitle).toBeInTheDocument();
      expect(cardImg).toBeInTheDocument();
    });
  });

  it('Should render tv show section & card with data fetched', async () => {
    await act(() => {
      render(
        <PageProvider>
          <Home />
        </PageProvider>
      );
    });
    const sectionTitle = screen.getByText(/movies/i);
    expect(sectionTitle).toBeInTheDocument();
    await waitFor(() => {
      const cardTitle = screen.getByText(mainShows.results[0].name);
      const cardImg = screen.getByAltText(mainShows.results[0].name);
      expect(cardTitle).toBeInTheDocument();
      expect(cardImg).toBeInTheDocument();
    });
  });

  it('Should render two multiple genres custom select', async () => {
    await act(() => {
      render(
        <PageProvider>
          <Home />
        </PageProvider>
      );
    });
    await waitFor(()=>{
      
      const genresCustomSelect = screen.getAllByLabelText(/multiple genres custom select/i);
      expect(genresCustomSelect).toHaveLength(2);
    });
  });

  it('Should render a single certification custom select', async () => {
    await act(() => {
      render(
        <PageProvider>
          <Home />
        </PageProvider>
      );
    });
    await waitFor(()=>{
      const genresCustomSelect = screen.getByLabelText(/single certification custom select/i);
      expect(genresCustomSelect).toBeInTheDocument();
    });
  });
});