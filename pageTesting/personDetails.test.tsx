import { screen, waitFor } from '@testing-library/dom';
import { act, render } from '@testing-library/react';
import PageProvider from '../testUtils/PageProvider';
import PersonDetails from '../pages/persons/[id]';
import personDetails from '../mocks/responses/personDetails.json';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
      query: { id: 1256603 },
      isReady: true
    };
  },
}));

describe('Test person details page', () => {
  it('Should render person details section & card with data fetched', 
    async () => {
      await act(() => {
        render(
          <PageProvider>
            <PersonDetails />
          </PageProvider>
        );
      });
      await waitFor(() => {
        const title = screen.getByText(personDetails.name);
        const posterImg = screen.getByAltText(personDetails.name);
        expect(title).toBeInTheDocument();
        expect(posterImg).toBeInTheDocument();
      });
    });
});