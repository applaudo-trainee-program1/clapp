import { Provider } from 'react-redux';
import { store } from '../redux/store';
import { MainLayout } from '../components';

export type PageProviderProps = {
  children: React.ReactNode
}

const PageProvider = ({children}:PageProviderProps) => {
  return (
    <Provider store={store}>
      <MainLayout>
        {children}
      </MainLayout>
    </Provider>
  );
};
export default PageProvider;