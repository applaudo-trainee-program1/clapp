import { useDispatch, useSelector } from 'react-redux';
import { setCertification, setGenres, setYear } from '../redux/slices/filter/filterSlice';
import { FilterState } from '../redux/slices/filter/types';
import { RootState } from '../redux/store';

export type FilterSliceTestProps = {
  newState: FilterState
}
const FilterSliceTest = ({newState}:FilterSliceTestProps) => {
  const dispatch = useDispatch();
  const filterState = useSelector((state: RootState) => state.filter);

  const updateMovieYear = (value:number | null) => {
    dispatch(setYear({
      year: value, 
      section: 'MOVIES'}));
  };

  const updateMovieGenres = (values:number[] | null) => {
    dispatch(setGenres({
      genres: (values as number[]), 
      section: 'MOVIES'}));
  };

  const updateMovieCertification = (values:string[] | null) => {
    dispatch(setCertification({
      certification: (values), 
      section: 'MOVIES'}));
  };

  const updateShowYear = (value:number | null) => {
    dispatch(setYear({
      year: value, 
      section: 'TV_SHOWS'}));
  };

  const updateShowGenres = (values:number[] | null) => {
    dispatch(setGenres({
      genres: (values as number[]), 
      section: 'TV_SHOWS'}));
  };

  return (
    <div>
      <button type='button' onClick={()=> updateMovieYear(newState['MOVIES'].year)}>
        Update movie year
      </button>
      <button type='button' onClick={()=> updateShowYear(newState['TV_SHOWS'].year)}>
        Update show year
      </button>
      <button type='button' onClick={()=> updateMovieGenres(newState['MOVIES'].genres)}>
        Update movie genres
      </button>
      <button type='button' onClick={()=> updateShowGenres(newState['TV_SHOWS'].genres)}>
        Update show genres
      </button>
      <button type='button' onClick={()=> updateMovieCertification(newState['MOVIES']
        .certification)}>
        Update movie certification
      </button>

      <span aria-label="currentMovieYear">{filterState['MOVIES'].year}</span>
      <span aria-label="currentMovieGenres">{JSON.stringify(filterState['MOVIES'].genres)}</span>
      <span aria-label="currentMovieCertification">
        {JSON.stringify(filterState['MOVIES'].certification)}
      </span>
      <span aria-label="currentShowYear">{filterState['TV_SHOWS'].year}</span>
      <span aria-label="currentShowGenres">
        {JSON.stringify(filterState['TV_SHOWS'].genres)}
      </span>
    </div>
  );
};
export default FilterSliceTest;