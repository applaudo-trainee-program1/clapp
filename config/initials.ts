import { FilterState } from '../redux/slices/filter/types';
import { LikeState } from '../redux/slices/like/types';
import { SignState } from '../redux/slices/sign/types';

export const initialFilterState : FilterState = {
  'MOVIES':{
    certification: null,
    genres: null,
    year: null
  },
  'TV_SHOWS':{
    certification: null,
    genres: null,
    year: null
  },
};

export const initialSignState : SignState = {
  isLogged: false
};

export const initialLikeState : LikeState = {
  MOVIES: [],
  TV_SHOWS: []
};