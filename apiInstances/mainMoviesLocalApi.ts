import {enviromentVariables} from '../utils';
import genericAxiosInstance from './genericApi';

const { mainMoviesLocalBaseUrl } = enviromentVariables;

const mainMoviesLocalApi = genericAxiosInstance({
  baseUrl: mainMoviesLocalBaseUrl || ''
});

export default mainMoviesLocalApi;
