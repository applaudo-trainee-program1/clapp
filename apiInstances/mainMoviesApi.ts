import {enviromentVariables} from '../utils';
import genericAxiosInstance from './genericApi';

const { mainMoviesBaseUrl } = enviromentVariables;

const mainMoviesApi = genericAxiosInstance({
  baseUrl: mainMoviesBaseUrl || ''
});

export default mainMoviesApi;
