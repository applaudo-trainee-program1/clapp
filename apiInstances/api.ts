import axios, { AxiosInstance, AxiosRequestConfig } from 'axios'
import { type } from 'os'
interface GenericAxiosInstanceProps {
  baseUrl: string
}
export interface ApiProps {
  baseUrl: string
}
export interface ConfigProps<D> {
  requestConfig: AxiosRequestConfig<D>
}

class Api<D = Object> {
  axiosInstance: AxiosInstance
  constructor ({ baseUrl }: ApiProps) {
    this.axiosInstance = this.genericAxiosInstance({ baseUrl })
  }

  genericAxiosInstance = ({ baseUrl }: GenericAxiosInstanceProps) => {
    const axiosInstance = axios.create({
      baseURL: baseUrl,
      headers: { 'Content-Type': 'application/json; charset=UTF-8' }
    })
    return axiosInstance
  }

  request = async ({ requestConfig }: ConfigProps<D>) => {
    const response = await this.axiosInstance(requestConfig)
    return response
  }
}

export default Api
