import { enviromentVariables } from '../utils';
import genericAxiosInstance from './genericApi';

const { searchMoviesTvShowsBaseUrl } = enviromentVariables;

const searchMoviesTvShowsApi = genericAxiosInstance({
  baseUrl: searchMoviesTvShowsBaseUrl || ''
});

export default searchMoviesTvShowsApi;
