import { enviromentVariables } from '../utils';
import genericAxiosInstance from './genericApi';

const { searchMoviesTvShowsLocalBaseUrl } = enviromentVariables;

const searchMoviesTvShowsLocalApi = genericAxiosInstance({
  baseUrl: searchMoviesTvShowsLocalBaseUrl || ''
});

export default searchMoviesTvShowsLocalApi;
