import { QueryParam } from '../ts/api';
import { 
  IsDefined, IsValidCastArrayOfNumberProps,  
  IsValidMovieCertificationProps, ValidateIntRangeProps, ValidMoviesCertifications, 
  validMoviesCertifications, 
  ValidShowsCertifications, 
  validShowsCertifications
} from './types';

export const isDefined = ({toValidate}:IsDefined) => {
  if(toValidate) return true;
  return false;
};

export type IsValidCastNumberProps = {
  toValidate: QueryParam
}
export const isValidCastNumber = ({toValidate}:IsValidCastNumberProps) => {
  if(!isDefined({toValidate}) || Number.isNaN(Number(toValidate)))
    return false;
  return Number(toValidate);
};

export const isValidCastArrayOfNumber = ({toValidate}:IsValidCastArrayOfNumberProps) => {
  if(!isDefined({toValidate}))
    return false;
  const toValidateString = String(toValidate);
  const toValidateArray = toValidateString.split(',');
  let toValidateArrayOfNumber:number[] = [];
  const isInvalidArray = toValidateArray
    .some( validateElement => {
      toValidateArrayOfNumber.push(Number(validateElement));
      return !isValidCastNumber({toValidate:validateElement});
    });
  if(!isInvalidArray) return toValidateArrayOfNumber;
  return false;
};
  
export const isValidMovieCertification = ({toValidate}:IsValidMovieCertificationProps) => {
  if(isDefined({toValidate}) && validMoviesCertifications
    .includes(toValidate as ValidMoviesCertifications))
    return true;
  return false;
};

export const isValidShowCertification = ({toValidate}:IsValidMovieCertificationProps) => {
  if(isDefined({toValidate}) && validShowsCertifications
    .includes(toValidate as ValidShowsCertifications))
    return true;
  return false;
};

/**
 * Validates an input field for integer numbers or ""
 *
 * @param {object} { value, min, max }
 * - value: input value
 * - min: minimun allowed value for that input
 * - max: maximum allowed value for that input
 * @return {value} value - integer number validated from min to max, or ""
 */
export const validateIntRange = ({ value, min, max }:ValidateIntRangeProps) => {
  if (value === '') return value;
  if (Number.isNaN(Number(value))) return min;
  if (value > min && value <= max)
    return Math.trunc(+value);
  value > max
    ? value = max
    : value = min;
  return Number(value);
};

export type isValidInputNumber = string 

export const isValidInputNumber = (value:isValidInputNumber) => {
  if(Number.isNaN(Number(value)) || !Number(value))
    return false;
  return true;
};

export const validateIntRangeBeforeDebounce = (value:string) => {
  const validatedBeforeDebounceValue = isValidInputNumber(value)
    ? value 
    : '';
  return validatedBeforeDebounceValue;
};

export const validateIntRangeWhileDebounce = (value:string, max: number, min: number) => {
  const validatedWhileDebounceValue = validateIntRange({
    max: max,
    min: min,
    value
  });
  return String(validatedWhileDebounceValue);
};