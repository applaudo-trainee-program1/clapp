export const GREAT_SCORE_BACKGROUND_CLASS = 'bg-teal-700';

export const MEDIUM_SCORE_BACKGROUND_CLASS = 'bg-yellow-600';

export const LOW_SCORE_BACKGROUND_CLASS = 'bg-rose-900';

export const NO_SCORE_BACKGROUND_CLASS = 'bg-black';

export const MOVIES_CERTIFICATION_ARRAY = [
  { header:'G', value: 'G' },
  { header:'NC-17', value: 'NC-17' },
  { header:'R', value: 'R' },
  { header:'PG', value: 'PG' },
  { header:'PG-13', value: 'PG-13' },
  { header:'NR', value: 'NR' }
];

export const MOVIES_GENRES_ARRAY = [
  { header:'Action', value: 28 },
  { header:'Adventure', value: 12 },
  { header:'Animation', value: 16 },
  { header:'Comedy', value: 35 },
  { header:'Crime', value: 80 },
  { header:'Documentary', value: 99 },
  { header:'Drama', value: 18 },
  { header:'Family', value: 10751 },
  { header:'Fantasy', value: 14 },
  { header:'History', value: 36 },
  { header:'Horror', value: 27 },
  { header:'Music', value: 10402 },
  { header:'Mystery', value: 9648 },
  { header:'Romance', value: 10749 },
  { header:'Science Fiction', value: 878 },
  { header:'TV Movie', value: 10770 },
  { header:'Thriller', value: 53 },
  { header:'War', value: 10752 },
  { header:'Western', value: 37 }
];

export const TV_SHOWS_GENRES_ARRAY = [
  { header:'Action & Adventure', value: 10759 },
  { header:'Animation', value: 16 },
  { header:'Comedy', value: 35 },
  { header:'Crime', value: 80 },
  { header:'Documentary', value: 99 },
  { header:'Drama', value: 18 },
  { header:'Family', value: 10751 },
  { header:'Kids', value: 10762 },
  { header:'Mystery', value: 9648 },
  { header:'News', value: 10763 },
  { header:'Reality', value: 10764 },
  { header:'Sci-Fi & Fantasy', value: 10765 },
  { header:'Soap', value: 10766 }
];

export const containerOpacityVariant = {
  hidden: { opacity: 0 },
  show: {
    opacity: 1,
    transition: {
      staggerChildren: 0.3
    }
  }
};

export const itemOpacityVariant = {
  hidden: { opacity: 0 },
  show: { opacity: 1 }
};

export const itemScaleVariant = {
  hidden: { scale: 0 },
  show: { scale: 1 }
};

export const itemHeightVariant = {
  hidden: { height: 0 },
  show: { height: 'auto' }
};