import { CardProps } from '../components';
import { Routes } from '../routes/routes';
import { QueryParam } from '../ts/api';
import { IDiscoverMovieResult, IDiscoverShowResult, IMovieDetails, IPeopleResult, 
  IPersonDetails, 
  IResponseDiscover, IResponseDiscoverMovies, IResponseDiscoverShows, 
  IResponseMainMovies, 
  ISeasonDetails, 
  ITvShowDetails} from '../ts/response';

export type GetMainRequestConfig = {
  page?: number,
  genres?: number[] | string,
  year?: number,
  certification?: string,
  section: Extract< keyof typeof Routes, 'MOVIES' | 'TV_SHOWS'> | 'BOTH',
}

export type ValidateIntRangeProps = {
  max: number,
  min: number,
  value: string | number
}

export type IsDefined = {
  toValidate: QueryParam
}

export type IsValidCastArrayOfNumberProps = {
  toValidate: QueryParam
}

export type IsValidMovieCertificationProps = {
  toValidate: QueryParam
}

export const validMoviesCertifications = ['R','PG','NC-17','G', 'NR', 'PG-13'] as const;

export type ValidMoviesCertifications = typeof validMoviesCertifications[number];

export const validShowsCertifications = ['NR','TV-Y','TV-Y7','TV-G', 'TV-PG', 'TV-14', 
  'TV-MA'] as const;

export type ValidShowsCertifications = typeof validShowsCertifications[number];

export type NormalizeLocalResponseProps = {
  results?: IResponseDiscoverMovies['results']
  section: 'MOVIES'
} |
{
  results?: IResponseDiscoverShows['results']
  section: 'TV_SHOWS'
} 

export type NormalizeLocalResponse = ({
  // eslint-disable-next-line no-unused-vars
  results, section
}:NormalizeLocalResponseProps) => (CardProps[] | null)

export type NormalizeMainMoviesResponseProps = {
  results?: IResponseMainMovies['results']
}

export type NormalizeMainMoviesResponseReturn = CardProps[] | null

export type NormalizeMainMoviesResponse = 
  // eslint-disable-next-line no-unused-vars
  ({results}:NormalizeMainMoviesResponseProps) 
  => NormalizeMainMoviesResponseReturn

export type NormalizeLocalSearchResponseProps = {
  results?: 
  IResponseDiscover<IDiscoverMovieResult>['results'] |
  IResponseDiscover<IDiscoverShowResult>['results'] |
  IResponseDiscover<IPeopleResult>['results']
}

export type GetLocalSearchRequestConfigProps = {
  page: number,
  query: string
}

export type getGenericDetailsRequestConfigProps = {
  id?: string,
  baseUrl: string
}

export type NormalizeMovieDetailsProps = {
  data: IMovieDetails
}

export type NormalizeTvShowDetailsProps = {
  data: ITvShowDetails,
  showId: number
}

export type NormalizeSeasonDetailsProps = {
  data: ISeasonDetails
}

export type GetSeasonDetailsRequestConfigProps = {
  flag: boolean,
  id: number,
  seasonNumber: number
}

export type NormalizePersonDetailsProps = {
  data: IPersonDetails
}

export type GetPersonDetailsRequestConfigProps = {
  flag: boolean,
  id: number
}